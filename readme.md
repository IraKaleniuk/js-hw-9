## Теоретичні питання

1. Опишіть, як можна створити новий HTML тег на сторінці.

   Новий HTML тег на сторінці можна створити за допомогою наступного методу:
   ```javascript
      document.createElement("tagName");
   ```
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

   ```javascript
      element.insertAdjacentHTML(where, html);
   ```
   Параметр where - це стрічка (спец. слово), яка вказує куди, відносно element, вставляти html. Може набувати наступних значень:

   - "beforebegin" - вставляє перед element;
   - "afterbegin" - вставляє на початок element;
   - "beforeend" - вставляє в кінець element;
   - "afterend" - вставляє після element.
   
3. Як можна видалити елемент зі сторінки?

   Видалити елемент зі сторінки можна за допомогою методу:
   ```javascript 
      element.remove();
   ``` 
   Є ще один метод, але він застарілий: 
   ```javascript 
      parent.removeChild(node);
   ``` 