"use strict";

function showList(arr, parent = document.body) {
    const ul = document.createElement("ul");
    const liArr = [];
    arr.forEach(el => {
        if (Array.isArray(el)) {
            showList(el, liArr[liArr.length - 1])
        } else {
            const li = document.createElement("li");
            li.textContent = el;
            liArr.push(li);
        }
    });
    ul.append(...liArr);
    parent.append(ul);
}

const arr1 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const el = document.querySelector("p");
showList(arr1);
showList(arr2, el);

const timerDiv = document.createElement("div");
timerDiv.className = "timer";
document.body.insertAdjacentElement("afterbegin", timerDiv);
let seconds = 3;
const timer = setInterval(() => {
    timerDiv.textContent = seconds -- + "s ";
    if (seconds < 0) {
        clearInterval(timer);
        document.body.innerHTML = "";
    }
}, 1000);
